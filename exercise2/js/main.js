(function () {
    var newCanvas,
    selectedShape,
    selectedCanvas,
    canvasArray = [],
    getRandomInt = fabric.util.getRandomInt(2, 5),
    canvasWrap = document.getElementById("canvas-wrap"),
    canvasList = document.getElementById('canvas-list'),
    optionsList = '<option disabled selected value>Canvas #</option>';
    
    //To render n amount of canvases
    for (var index = 0; index < getRandomInt; index++) {
        // A canvas tag is required or Fabric.js doesn't know where to draw. 
        newCanvas = document.createElement("canvas"); 
        canvasWrap.appendChild(newCanvas);
        var gradCanvas = new fabric.Canvas(newCanvas, {width: 400, height:400});   // To do add the border

        // Create a text object.
		var textObj = new fabric.Text("canvas "+ (index + 1), {
			left: 0,
			top: 0		
		});
	
		// Attach it to the canvas object
		gradCanvas.add(textObj);

        canvasArray.push(gradCanvas);
        
        optionsList += '<option value="'+ index + '">canvas' + (index + 1) + '</option>'; 
        

    }

    canvasList.innerHTML = optionsList;

    var addShape = document.getElementById('canvas-cta');

    document.getElementById('shape-list').addEventListener("change", function(e) {
        selectedShape = e.target.value;
    });  

    document.getElementById('canvas-list').addEventListener("change", function(e) {
        selectedCanvas = e.target.value;
    });   

    addShape.addEventListener("click", addShapeToCanvas);

    //To add shape to the selected canvas
    function addShapeToCanvas(e) {
        // To enable the validation on submit
        if(selectedShape && selectedCanvas) {
            e.preventDefault();
        } 

        var positionTop = fabric.util.getRandomInt(0, 100),
        positionLeft = fabric.util.getRandomInt(0, 200);

        if(typeof(selectedShape) != "undefined" && typeof(selectedCanvas) !== "undefined") { 
            switch (selectedShape) {
                case "rect":
                    canvasArray[selectedCanvas].add(new fabric.Rect({
                        top : positionTop,
                        left : positionLeft,
                        width : 60,
                        height : 70,
                        fill : 'red'
                    }));
                    break;
                case "circle":
                    canvasArray[selectedCanvas].add(new fabric.Circle({
                        top : positionTop,
                        left : positionLeft,
                        width : 200,
                        height : 200,
                        fill : 'blue',
                        radius: 30
                    }));
                    break;
                case "triangle":
                    canvasArray[selectedCanvas].add(new fabric.Triangle({
                        width: 60,
                        height: 70,
                        left: positionLeft,
                        top: positionTop,
                        fill: 'green'
                    }));
                    break
            
                default:
                    break;
            }
        }
    }

    var activeObject, drawImage;

    //To Drag and drop canvas objects
    canvasArray.map(function(canvas, index){
        canvas.observe('object:moving', function(event) {
            activeObject = this.getActiveObject();

                if(activeObject && (activeObject.top < 0 || activeObject.top > this.height)) {
                    drawImage  = fabric.util.object.extend({},this.getActiveObject());
                    this.remove(activeObject);
                }
            
        });                        
    })

    //mouseup event:
    document.addEventListener("mouseup", function(event) {
        if (drawImage != null) {
            canvasArray.map(function (canvas) {
                if (Intersect([event.pageX, event.pageY],canvas.wrapperEl)) {
                    drawImage.left = event.pageX - canvas.wrapperEl.offsetLeft;
                    drawImage.top = event.pageY - canvas.wrapperEl.offsetTop;
                    canvas.add(drawImage);
                }
            });

            drawImage  = null;
        }
    })

    //Helper Intersect function:    
    function Intersect(point, canvas) {
        var top = point[1],
        left = point[0];
        return ( left > canvas.offsetLeft
            && left < canvas.offsetLeft + canvas.offsetWidth
            && top < canvas.offsetTop + canvas.offsetHeight
            && top > canvas.offsetTop
        );            
    }

})();